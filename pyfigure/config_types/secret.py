from os import environ

from .base import BaseType


class Secret(BaseType):

    def wrapper(key: str):

        try:
            return environ[key]
        except KeyError:
            raise KeyError(f"Could not find an environmental variable for the key '{key}'. Maybe you haven't imported your variables from your .env file?")
