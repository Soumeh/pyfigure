from datetime import datetime

from .base import BaseType

class DateTime(BaseType):

    def wrapper(value: str, date_format: str = '%d/%m/%y %H:%M:%S'):
        return datetime.strptime(value, date_format)

class Time(DateTime):

    def wrapper(value: str, date_format: str = '%H:%M:%S'):
        return datetime.strptime(value, date_format)

class Date(DateTime):

    def wrapper(value: str, date_format: str = '%d/%m/%y'):
        return datetime.strptime(value, date_format)