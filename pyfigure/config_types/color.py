from typing import Union

from .base import BaseType


#class Number(BaseType):
#
#    def wrapper(number: Number, min: Number = None, max: Number = None):
#
#        if min is not None and number < min:
#            raise ValueError(f'Value must be larger than {min}')
#        elif max is not None and number > max:
#            raise ValueError(f'Value must be no larger than {max}')
#
#        elif min is not None and max is not None:
#            if number < min:
#                raise ValueError(f'Value must be larger than {min}')
#            if number > max:
#                raise ValueError(f'Value must be no larger than {max}')
#
#        return number

class Color:

    red: int
    green: int
    blue: int

    _colors = {
        'red':   'ff0000',
        'green': '00ff00',
        'blue':  '0000ff',
    }

    def from_int(self, value: int):
        self.blue = value & 255
        self.green = (value >> 8) & 255
        self.red = (value >> 16) & 255
        self.alpha = (value >> 24) & 255

    def from_str(self, value: str):
        value = value.lstrip('#').lower()
        value = self._colors.get(value, value)

        color_values = [ int(value[i:i + 2], 16) for i in range(0, len(value), 2)]
        while len(color_values) < 3:
            color_values += [0]
        self.red, self.green, self.blue, self.alpha = color_values + [None]

    def from_list(self, value: list):
        value = tuple(value)

        while len(value) < 3:
            value += (0,)
        self.red, self.green, self.blue, self.alpha, *_ = value + (None,)

    def __init__(self, value: Union[int, str, list]):

        if isinstance(value, int):
            self.from_int(value)
        elif isinstance(value, str):
            self.from_str(value)
        elif isinstance(value, (list, tuple)):
            self.from_list(value)
        else:
            raise TypeError("Value must be an integer, string, or list object")
        
        self._validate()

    def _validate(self):
        for color, color_value in {
            'red': self.red,
            'green': self.green,
            'blue': self.blue,
            'alpha': self.alpha
        }.items():
            if color_value and color_value > 255:
                raise ValueError(f"Value of {color} must not be larger than 255")


    def as_rgb(self):
        """Returns the RGB value of the color as a tuple of integers."""
        return (self.red, self.green, self.blue)
    rgb = property(as_rgb)

    def __str__(self):
        result = '%02X%02X%02X' % (self.red, self.green, self.blue)
        if self.alpha:
            result += "%02X" % self.alpha
        return result
    
    def __int__(self):
        result = self.red
        result = (result << 8) + self.green
        result = (result << 8) + self.blue
        if self.alpha:
            result = (result << 8) + self.alpha
        return result