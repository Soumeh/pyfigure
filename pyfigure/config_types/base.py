from typing import Any

class BaseType:

    #args: dict = {}

    def instantiator(self, **wrapper_args):
        return self.wrapper()

    def wrapper(value: Any):
        pass

    def __new__(self, *args, **kwargs):
        if args:
            return self.wrapper(*args, **kwargs)
        return lambda x: self.wrapper(x, **kwargs)