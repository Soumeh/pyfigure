from numbers import Number as NumberType

from .base import BaseType

class Number(BaseType):

    def wrapper(
        number: NumberType,
        min: NumberType = None,
        max: NumberType = None
    ):

        if min is not None and number < min:
            raise ValueError(f'Value must be larger than {min}')
        elif max is not None and number > max:
            raise ValueError(f'Value must be no larger than {max}')

        elif min is not None and max is not None:
            if number < min:
                raise ValueError(f'Value must be larger than {min}')
            if number > max:
                raise ValueError(f'Value must be no larger than {max}')

        return number