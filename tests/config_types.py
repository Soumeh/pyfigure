from os import environ
from pyfigure.config_types import (
    Date, Time, DateTime,
    Color,
    Secret,
    Number,
)

def test_time():

    assert '21/04/03' == Date('21/04/03').strftime('%d/%m/%y')
    assert '12:00:00' == Time('12:00:00').strftime('%H:%M:%S')
    assert '21/04/03 12:00:00' == DateTime('21/04/03 12:00:00').strftime('%d/%m/%y %H:%M:%S')

def test_colors():

    color = Color('00FF00')

    assert color.red == 0
    assert color.green == 255
    assert color.blue == 0

    color.blue = 255

    assert color.blue == 255
    assert str(color) == '00FFFF'
    assert int(color) == 65535
    assert color.rgb == (0, 255, 255)

    # TODO: Validate the color value

    #failed = False
    #try:
    #    color.red = 256
    #except ValueError:
    #    failed = True
    #
    #assert failed == True

def test_secrets():
    
    environ['TESTING'] = "true"
    assert Secret('TESTING') == "true"

    failed = False
    try:
        secret = Secret('THIS_ENV_VAR_SHOULD_NOT_EXIST')
    except KeyError:
        failed = True
    assert failed == True

def test_numbers():

    validator = Number(min=10, max=50)

    failed = False
    try:
        validator(5)
    except ValueError:
        failed = True
    assert failed == True

    failed = False
    try:
        validator(100)
    except ValueError:
        failed = True
    assert failed == True

    assert validator(15) == 15

    assert Number(99) == 99
